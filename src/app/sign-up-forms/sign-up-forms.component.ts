import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms'

@Component({
  selector: 'app-sign-up-forms',
  templateUrl: './sign-up-forms.component.html',
  
})
export class SignUpFormsComponent implements OnInit {


  formSubmitted = false;

  mojaForma: FormGroup;

  

  constructor() { 
    this.mojaForma = new FormGroup ({
      ime: new FormControl('',Validators.required),
      prezime:new FormControl('',Validators.required),
      datumRodjenja: new FormControl(''),
      adresa: new FormGroup({
        grad: new FormControl(''),
        ulica: new FormControl(''),
        broj: new FormControl(''),
        postanskiBroj: new FormControl('')
        
      }),
      pol: new FormControl(''),  
      saglasnost: new FormControl(false, Validators.required)

    });
  }

  ngOnInit() {
  }

 

  consoleLoguj() {
    console.log(this.mojaForma);
    this.formSubmitted = true;
  }

  handleSubmit(e) {
    e.preventDefault();
    if(this.mojaForma.valid) {

    }
  }

}
