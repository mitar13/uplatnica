import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms'

@Component({
  selector: 'app-uplatnica',
  templateUrl: './uplatnica.component.html',
 
})
export class UplatnicaComponent implements OnInit {

  formSubmitted = false;

  mojaForma: FormGroup;

  

  constructor() { 
    this.mojaForma = new FormGroup ({
      uplatilac: new FormGroup({
        imeIPrezime:new FormControl('',Validators.required),
        adresaPrebivalista:new FormControl('')
      }),
      svrhaUplate:new FormControl('',Validators.required),
      datumRodjenja: new FormControl(''),
      primalac: new FormControl(''),
      svojerucniPotpis: new FormControl(''),
      prazno: new FormControl(''),  
      rsd: new FormControl(''),
      iznos: new FormControl(''),
      ziroRacun: new FormControl(''),
      prazno2: new FormControl(''),
      pozivNaBroj: new FormControl('')

    });
  }

  ngOnInit() {
  }

 

  consoleLoguj() {
    console.log(this.mojaForma);
    this.formSubmitted = true;
  }

  handleSubmit(e) {
    e.preventDefault();
    if(this.mojaForma.valid) {

    }
  }

}
