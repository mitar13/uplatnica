import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import { SignUpFormsComponent } from './sign-up-forms/sign-up-forms.component';
import { UplatnicaComponent } from './uplatnica/uplatnica.component';

@NgModule({
  declarations: [
    AppComponent,
    SignUpFormsComponent,
    UplatnicaComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
